# pytab class

A python class to read kerberos keytab and credential cache files.

# minikdc

The mini KDC creates a keytab for a server and also creates a credential cache file.
With a credential cache file in place, you are logged in to the kerberos realm.

## Installation

* apt install python3-pyasn1 python3-impacket

## Create keytab and credential cache

* minikdc.py --keytab krb5.keytab --ccache /tmp/krb5cc_1000 --realm INTERNAL.LAN --username user --fqdn server.internal.lan
