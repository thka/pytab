#!/usr/bin/env python3

import sys
sys.path.append('../')
from pytab.pytab import Keytab, Keyentry, Principal


def test_keytab_class():
    keytab_file = Keytab()
    assert keytab_file.load('file_not_found') is False
    assert keytab_file.load('salt-INTERNAL.LANhosttest.internal.lan-password-foo123.keytab') is True
    assert len(keytab_file.entries()) == 1
    assert isinstance(keytab_file.entry(0), Keyentry) is True
    assert keytab_file.export() == b'\x05\x02\x00\x00\x00J\x00\x02\x00\x0cINTERNAL.LAN\x00\x04host\x00\x11test.internal.lan\x00\x00\x00\x01c\xf2\x8a/\x02\x00\x11\x00\x10\x05l\x03\xb4-\xfa\x9d\xe4\x1eC\\\x1a\xf9\x13\xf8\xf7\x00\x00\x00\x02'
    keytab_file.purge(0)
    assert len(keytab_file.entries()) == 0


def test_keyentry():
    keytab = Keytab()
    keytab.load('salt-INTERNAL.LANhosttest.internal.lan-password-foo123.keytab')
    entry = keytab.entry(0)
    assert entry.timestamp == 1676839471
    assert entry.kvno == 2
    assert entry.kvno_extended == 2
    assert entry.encryption_type == 17
    assert entry.encryption_name == 'aes128-cts-hmac-sha1-96'
    assert entry.key == b'\x05l\x03\xb4-\xfa\x9d\xe4\x1eC\\\x1a\xf9\x13\xf8\xf7'
    assert entry.key_base64 == 'BWwDtC36neQeQ1wa+RP49w=='


def test_principal():
    keytab = Keytab()
    keytab.load('salt-INTERNAL.LANhosttest.internal.lan-password-foo123.keytab')
    entry = keytab.entry(0)
    principal = entry.principal
    assert principal.components == ['host', 'test.internal.lan']
    assert principal.realm == 'INTERNAL.LAN'
    assert principal.spn() == 'host/test.internal.lan@INTERNAL.LAN'
    assert principal.name_type == 1
    assert principal.name_type_name() == 'KRB_NT_PRINCIPAL'
    assert principal.export() == b'\x00\x02\x00\x0cINTERNAL.LAN\x00\x04host\x00\x11test.internal.lan\x00\x00\x00\x01'

    principal.name_type = 99
    assert principal.name_type_name() == 'unknown-name-type'

    principal.realm = 'OTHER.LAN'
    assert principal.realm == 'OTHER.LAN'

def test_create_keytab():
    original_keytab = Keytab()
    original_keytab.load('salt-INTERNAL.LANhosttest.internal.lan-password-foo123.keytab')

    new_principal = Principal()
    new_principal.realm = ('INTERNAL.LAN')
    new_principal.components.append('host')
    new_principal.components.append('test.internal.lan')
    new_principal.name_type = 1

    new_keyentry = Keyentry()
    new_keyentry.principal = new_principal
    new_keyentry.timestamp = 1676839471
    new_keyentry.kvno = 2
    new_keyentry.kvno_extended = 2
    new_keyentry.encryption_type = 17
    new_keyentry.password('foo123')

    new_keytab = Keytab()
    new_keytab.key_entries.append(new_keyentry)

    assert original_keytab.key_entries[0].principal.realm == new_keytab.key_entries[0].principal.realm
    assert original_keytab.key_entries[0].principal.components[0] == new_keytab.key_entries[0].principal.components[0]
    assert original_keytab.key_entries[0].principal.components[1] == new_keytab.key_entries[0].principal.components[1]
    assert original_keytab.key_entries[0].timestamp == new_keytab.key_entries[0].timestamp
    assert original_keytab.key_entries[0].kvno == new_keytab.key_entries[0].kvno
    assert original_keytab.key_entries[0].kvno_extended == new_keytab.key_entries[0].kvno_extended
    assert original_keytab.key_entries[0].key == new_keytab.key_entries[0].key
